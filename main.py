import telebot
from telebot import apihelper
import vk_api
import json
import requests
from lxml import html
import re
import sqlite3
from datetime import datetime
import os

conn = sqlite3.connect("mydatabase.db")


def auth_handler():
    """ При двухфакторной аутентификации вызывается эта функция.
    """

    # Код двухфакторной аутентификации
    key = input("Enter authentication code: ")
    # Если: True - сохранить, False - не сохранять.
    remember_device = True

    return key, remember_device


def url_finder(html_file):
    array_of_index = [u'"url720":"(.+?)"', u'"url480":"(.+?)"', u'"url240":"(.+?)"']
    array_of_find_dict = {'video_720': '', 'video_480': '', 'video_240': ''}
    s = 0
    for i in array_of_index:
        find_url = re.findall(i, html_file)
        if find_url:
            end_url = re.findall(u'^(.+?)\?', find_url[0])
            end_url = re.sub(r"\\", "", end_url[0])
            if s == 0:
                array_of_find_dict['video_720'] = end_url
            elif s == 1:
                array_of_find_dict['video_480'] = end_url
            elif s == 2:
                array_of_find_dict['video_240'] = end_url
        s = s+1
   # print(array_of_find_dict)
    return array_of_find_dict


def insert_in_db(values_list):
    # print(values_list)
    sql = ''' INSERT INTO tg_db(id,owner_id,video_id,posted,date,video_720,video_480,video_240)
                  VALUES(?,?,?,?,?,?,?,?) '''
    with conn:
        cur = conn.cursor()
        cur.execute(sql, values_list)
        conn.commit()


def update_bd(id):
    list = {'id': id,
            'values': {
            'posted': 1,
            'date': str(datetime.now())
            }
         }
    s = ''
    for i in list['values']:
        # print(i + ': ' + str(list['values'][i]))
        # print(i + ': ' + list['values'][i])
        if i != 'posted':
            s = s + i + ' = ' + '"' + str(list['values'][i]) + '", '
        else:
            s = s + i + ' = ' + str(list['values'][i]) + ', '
        # print(i + ': ' + str(list['values'][i]))
    s = s[0:-2]
    sql = 'UPDATE tg_db SET ' + s + ' WHERE id = ' + '"' + list['id'] + '"'
    with conn:
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
    # print(sql)


def check_sql(id_bd):
    sql = 'SELECT COUNT(*) FROM tg_db WHERE id=' + '"' + id_bd + '"'
    # print(sql)
    with conn:
        cur = conn.cursor()
        cur.execute(sql)
        s = cur.fetchall()
    return s[0][0]


def main():
    access_token_vk = "202ff2862fc6c8ceaa60226d8ea9bc67eb5a5798c609b9c9294d8de488b76d9aa4693cd14daeecefc65be"
    login, password = 'dmitry.leskin@gmail.com', 'Freezee_man2018'
    vk_session = vk_api.VkApi(
        login, password,
        token=access_token_vk,
        # функция для обработки двухфакторной аутентификации
        auth_handler=auth_handler,
        api_version='5.103'
    )
    #print(vk_session)
    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()
    stats = vk.stats.trackVisitor()
    # print(stats)
    response = vk.wall.get(owner_id="-142308683", count=10, offset=1)  # Используем метод wall.get

    video_id = str(response['items'][0]['attachments'][0]['video']['owner_id']) + '_' + str(
        response['items'][0]['attachments'][0]['video']['id'])
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134'
    }
    video_start = 'https://vk.com/video'
    #print(video_id)
    sql_data = {
        'id': video_id,
        'owner_id': str(response['items'][0]['attachments'][0]['video']['owner_id']),
        'video_id': str(response['items'][0]['attachments'][0]['video']['id']),
        'posted': 0,
        'date': str(datetime.now())
    }
    response_url = requests.get(video_start + video_id, headers=headers)
    arr_of_find = url_finder(response_url.text)
    # print(arr_of_find)
    p = []
    for i in arr_of_find:
        sql_data.update({i: arr_of_find[i]})
    for i in sql_data:
        p.append(sql_data[i])
    # print(p)
    if (check_sql(p[0])==0):
        insert_in_db(p)
        # f = input("1)720p - %s \n2)480p - %s\n3)240p - %s\nChoose:\n" % (arr_of_find['720'], arr_of_find['480'], arr_of_find['240']))
        # print(f)
        if (p[5]!=''):
            print('720')
            r = requests.get(p[5], allow_redirects=True)
        elif (p[6]!=''):
            print('480')
            r = requests.get(p[6], allow_redirects=True)
        else:
            print('240')
            r = requests.get(p[7], allow_redirects=True)

        open(video_id + '.mp4', 'wb').write(r.content)
        API_TOKEN = '1027731313:AAETnOA3Bt78wOMEXZM1PPxio8dD6lmzuxw'
        #
        apihelper.proxy = {'https': 'socks5://12628593:oIanegrB@orbtl.s5.opennetwork.cc:999'}
        bot = telebot.TeleBot(API_TOKEN)
        video_tg = open(video_id + '.mp4', 'rb')
        #
        chanel_name = '@tiktoktrands'
        # print("tst")
        bot.send_video(chanel_name, video_tg)
        update_bd(p[0])


if __name__ == '__main__':
    main()
